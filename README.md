<div align="center"><img src="https://upload.wikimedia.org/wikipedia/commons/5/56/VSCodium_Logo.png" width="200px"></div><br />

# VSCodium Backup

> This repo contains my codium settings, since sync feature is disabled in codium, I am using this repo to backup my settings and extensions as well.

## Backup Script

- It's written in Go, you can run it with;
  - `go run backup.go`
- After that it is pretty simple and self explanatory. 
- It takes backup of your settings to this directory.
- It saves your extensions to [codiumExtensions.md](codiumExtensions.md)
- It can load your backup settings to codium.
- It can auto install all of your extensions from [codiumExtensions.md](codiumExtensions.md).

Alternatively you can **download binaries** from [releases/tags](https://gitlab.com/bw3u/vscodium-backup/-/tags).
## License

This project is under the [MIT](LICENSE.md) License.
